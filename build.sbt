import Dependencies._
import build._


Global / onChangedBuildSource := ReloadOnSourceChanges

lazy val `sttp-bench` = Project(
  id = "sttp-bench",
  base = file("sttp-bench")
).settings(
  commonSettings,
  Seq(
    libraryDependencies ++= `sttp-core` ++ `sttp-async-client-fs2` ++ `sttp-http4s` ++ `logging-runtime`,
  )
).enablePlugins(JavaAppPackaging, UniversalPlugin)