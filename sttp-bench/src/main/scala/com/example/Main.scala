package com.example

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import sttp.client3._
import sttp.model._
import fs2.Stream
import cats.effect.IO
import sttp.client3.asynchttpclient.fs2.AsyncHttpClientFs2Backend
import cats.effect.unsafe.implicits.global
import org.asynchttpclient.{DefaultAsyncHttpClient, DefaultAsyncHttpClientConfig}
import cats.effect.std.Dispatcher
import java.nio.file.Paths
import sttp.monad.MonadError
import sttp.monad.syntax._
import sttp.client3.impl.cats.implicits._
import sttp.client3.logging.slf4j.Slf4jLoggingBackend
import org.slf4j.LoggerFactory

val logger = LoggerFactory.getLogger("Main")

@main
def main() =
  println("Hello world")
  val accessKey = "user"
  val secret = "password"
  val uri = "https://localhost:9505"
  val bucket = "testbucket"
  val path = "/tmp/rand-500mb"
  val signature = AwsSign.signRequest(
    "PUT",
    "application/octet-stream",
    LocalDateTime.ofInstant(Instant.EPOCH, ZoneId.of("UTC")),
    "/bucket/test-file",
    secret
  )
  println(s"Signature: $signature")

  // next:
  println("async http backend")
  Dispatcher[IO]
    .use { dispatcher =>
      given client: SttpBackend[IO, Any] = Slf4jLoggingBackend(SttpBench.asyncClientBackend()(using dispatcher))
      SttpBench.uploadFileS3Compat[IO](uri, bucket, path, accessKey, secret).map(_ => client.close())
    }
    .unsafeRunSync()

object SttpBench:
  // oh that's nice, no semver: see comment https://github.com/AsyncHttpClient/async-http-client/issues/1501
  def asyncClientBackend()(using disp: Dispatcher[IO]): SttpBackend[IO, Any] =
    AsyncHttpClientFs2Backend
      .usingConfigBuilder(
        disp,
        c => c.setDisableHttpsEndpointIdentificationAlgorithm(true).setUseInsecureTrustManager(true)
      )
      .unsafeRunSync()

  def uploadFileS3Compat[F[_]: MonadError](
      url: String,
      bucket: String,
      path: String,
      accessKey: String,
      secret: String
  )(using backend: SttpBackend[F, Any]): F[Unit] =
    val filePath = Paths.get(path)
    val resource = s"/$bucket/$path"
    val date = LocalDateTime.ofInstant(Instant.now(), ZoneId.of("UTC"))
    val bucketUrl = Uri.unsafeParse(s"$url$resource")

    val signature = AwsSign.signRequest("PUT", "application/octet-stream", date, resource, secret)

    val req = basicRequest
      .put(bucketUrl)
      .header("Authorization", s"AWS $accessKey:$signature")
      .header("Date", AwsSign.AmazonDateTimeFormatter.format(date))
      .header("Host", "localhost")
      .body(filePath)

    req
      .send(backend)
      .map { resp =>
        logger.info(s"Received response: $resp")
      }
