package com.example

import java.net.URLEncoder
import java.security.MessageDigest
import java.time._
import java.time.format.DateTimeFormatter
import java.util.Base64
import javax.crypto.Mac
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

object AwsSign:
  val AmazonDateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss'Z'")

  // Simple version of the signing - see https://docs.aws.amazon.com/general/latest/gr/sigv4-signed-request-examples.html for a fuller example
  def signRequest(method: String, contentType: String, date: LocalDateTime, resource: String, secret: String): String =
    val s = s"$method\n\n${contentType}\n${AmazonDateTimeFormatter.format(date)}\n${resource}"
    val key = new SecretKeySpec(secret.getBytes, "HmacSHA1")
    val signed = signWithKey(key, s.getBytes, "HmacSHA1")
    val encoded = Base64.getEncoder().encode(signed)
    new String(encoded)

  private def signWithKey(
      key: SecretKeySpec,
      bytes: Array[Byte],
      algorithm: String
  ): Array[Byte] = {
    val mac = Mac.getInstance(algorithm)
    mac.init(key)
    mac.doFinal(bytes)
  }
