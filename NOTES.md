
## With a 500MB random file

See https://gitlab.com/vladimir-lu/hyper-upload-bench/-/blob/main/NOTES.md for setup info

With everything untuned, on linux, with graalvm 21.3 JDK 17.

### async-http-client

Unwarm VM:

```
10:22:07.628 [io-compute-6] DEBUG sttp.client3.logging.slf4j.Slf4jLoggingBackend - Request: PUT https://localhost:9505/testbucket//tmp/rand-500mb, took: 2.091s, response: 200 OK, headers: Accept-Ranges: bytes, Content-Security-Policy: block-all-mixed-content, ETag: "cc8ce08e82dff58453ab8e4da7533b1c", Server: MinIO, Strict-Transport-Security: max-age=31536000; includeSubDomains, Vary: Origin, Vary: Accept-Encoding, X-Amz-Request-Id: 16B7ADD65CD475AC, X-Content-Type-Options: nosniff, X-Xss-Protection: 1; mode=block, Date: Mon, 15 Nov 2021 09:22:07 GMT, content-length: 0
```

2.091s aka 244 MB/s

### http4s-blaze

