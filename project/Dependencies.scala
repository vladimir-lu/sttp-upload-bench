import sbt._

object Dependencies {

  object Versions {
    val logback = "1.2.7"
    val sttp = "3.3.16"
  }

  val `sttp-core` = Seq(
    "com.softwaremill.sttp.client3" %% "core" % Versions.sttp,
    "com.softwaremill.sttp.client3" %% "cats" % Versions.sttp,
    "com.softwaremill.sttp.client3" %% "slf4j-backend" % Versions.sttp,
  )

  val `sttp-async-client-fs2` = Seq(
    "com.softwaremill.sttp.client3" %% "async-http-client-backend-fs2" % Versions.sttp,
  )

  val `sttp-http4s` = Seq(
    "com.softwaremill.sttp.client3" %% "http4s-backend" % Versions.sttp,
  )

  val `logging-runtime` = Seq(
    "ch.qos.logback" % "logback-classic" % Versions.logback,
  ).map(_ % "runtime")
}
